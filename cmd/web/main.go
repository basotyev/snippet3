package main

import (
	"context"
	"flag"
	"github.com/golangcollege/sessions"
	"github.com/jackc/pgx/v4/pgxpool"
	"html/template"
	"log"
	"net/http"
	"os"
	"snippet3/pkg/models/postresql"
	"time"
)

type application struct {
	iLog          *log.Logger
	eLog          *log.Logger
	snippets      *postresql.SnippetModel
	templateCache map[string]*template.Template
	session       *sessions.Session
}

func main() {
	abdr := flag.String("abdr", ":4000", "HTTP network address")
	dsn := flag.String("dsn", "postgres://postgres:0000@localhost:5432/SnippetSQL", "pg data source name")
	secret := flag.String("secret", "s6Ndh+pPbnzHbS*+9Pk8qGWhTzbpa@ge", "Secret key")

	flag.Parse()

	iLog := log.New(os.Stdout, "Info:\n", log.Ldate|log.Ltime)
	eLog := log.New(os.Stderr, "Errors:\n", log.Ldate|log.Ltime|log.Lshortfile)

	db, err := con(*dsn)
	if err != nil {
		eLog.Fatal(err)
	}
	defer db.Close()

	templateCache, err := newTemplateCache("./ui/html/")
	if err != nil {
		eLog.Fatal(err)
	}
	session := sessions.New([]byte(*secret))
	session.Lifetime = 12 * time.Hour
	app := application{
		eLog:          eLog,
		iLog:          iLog,
		snippets:      &postresql.SnippetModel{DataBase: db},
		templateCache: templateCache,
		session:       session,
	}

	srv := &http.Server{
		Addr:     *abdr,
		ErrorLog: eLog,
		Handler:  app.routes(),
	}

	iLog.Printf("Starting server on %s", *abdr)
	err = srv.ListenAndServe()
	eLog.Fatal(err)
}

func con(dsn string) (*pgxpool.Pool, error) {
	db, err := pgxpool.Connect(context.Background(), dsn)
	if err != nil {
		return nil, err
	}
	return db, nil
}
